---
author:  Equipe Mathématique Lycée Les Rimains
title: Conditions
---
# Structures Conditionnelles en Python



## I. Introduction

Dans de nombreux programmes, il est nécessaire de définir des actions en fonction du résultat d'un ou plusieurs conditions.

Dans les exemples ci-dessous, `conditions` est de type booléen et `action` correspond à  une ou plusieurs instructions

!!! info "Attention à la syntaxe"
    * les lignes contenant les conditions se terminent toujours par `:`
    
    * dans les lignes qui suivent la condition, les instructions sont indentées (décalées vers la droite)

## II. Structure conditionnelle simple :

```python
if condition :
    action1
```

Dans ce cas, l'action1 sera exécutée si et seulement si la condition est vraie ( condition == True) . Si la condition n'est pas vraie, aucune action ne sera réalisée. 


## III. Structure conditionnelle double :

```python
if condition :
    action1
else :
    action2
```
Dans le cas de la structure conditionnelle double, si la condition est vraie, l'action1 sera exécutée et sinon, c'est l'action2 qui sera exécutée

!!! info "Remarque "
    Attention, pas de condition pour le `else` !

## IV Structure conditionnelle multiple

```python
if condition1 :
    action1
elif condition2 : 
    action2
elif condition3 :
    action3
else :
    action4
```
Comme vous pouvez le constater, les différentes conditions seront testées et si aucune n'est vérifiée, c'est l'action4 qui sera exécutée.

## V. Application


???+ question "Exercice 1 papier"

    ```python linenums='1'
    a = 10
    b = 13
    if a > 5:
        b = b - 4
    if b >= 11:
        b = b + 10
    ```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 13
        - [ ] 9
        - [ ] 19
        - [ ] 23

    === "Solution"

        - :x: ~~13~~ Faux car 10 > 5. La ligne 4 et donc exécutée.
        - :white_check_mark: 9 
        - :x: ~~19~~ 
        - :x: ~~23~~ Faux car à la ligne 4 on a une nouvelle affectation de b qui ne respecte plus la condition du if de la ligne 5.


???+ question "Exercice 2 'papier'"

    ```python linenums='1'
    a = 5
    b = 14
    if a > 9:
        b = b - 1
    if b >= 10:
        b = b + 8
    ```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 22
        - [ ] 21
        - [ ] 13
        - [ ] 14

    === "Solution"

        - :white_check_mark: 22 
        - :x: ~~21~~ Faux car la ligne 4 n'est pas exécutée
        - :x: ~~13~~ Faux car la ligne 4 n'est pas exécutée
        - :x: ~~14~~ Faux car à la ligne 6 est exécutée

???+ question "Exercice 3 'papier'"

    ```python linenums='1'
    a = 7
    b = 20
    if a > 5:
        b = b - 3
    if b >= 15:
        b = b + 7
    ```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 24
        - [ ] 17
        - [ ] 20
        - [ ] 27

    === "Solution"

        - :white_check_mark: 24 
        - :x: ~~17~~ Faux car la ligne 6 est aussi exécutée
        - :x: ~~20~~ Faux car la ligne 4 et la ligne 6 sont exécutées.
        - :x: ~~27~~ Faux car à la ligne 4 est exécutée

???+ question "Exercice 4 'papier'"

    ```python linenums='1'
    a = 2
    b = 14
    if a >= 0:
        b = b - 2
    else:
        b = b + 2	
    if b > 0:
        a = a + 1
    else:
        a = a - 1
    ```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 3
        - [ ] 16
        - [ ] 1
        - [ ] 12

    === "Solution"

        - :white_check_mark: 3 
        - :x: ~~16~~ Faux car la ligne 6 n'est pas exécutée.
        - :x: ~~1~~ Faux car la ligne 10 n'est pas exécutée.
        - :x: ~~12~~ Faux car on cherche la valeur finale de a.

???+ question "Exercice 5 'papier'"

    ```python linenums='1'
    a = -3
    b = -11
    if a >= 0:
        b = b - 1
    else:
        b = b + 1	
    if b > 0:
        a = a + 3
    else:
        a = a - 3
    ```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] -10
        - [ ] -6
        - [ ] 0
        - [ ] -12

    === "Solution"

        - :x: ~~-10~~ La ligne 6 est exécutée, mais on demande a.
        - :white_check_mark: -6. La ligne 6 et la ligne 10 sont exécutée. On demande a.
        - :x: ~~0~~ Faux car la ligne 8 n'est pas exécutée.
        - :x: ~~-12~~ 

???+ question "Exercice 6"

	=== "Énoncé"

        On chercher à écrire un programme qui demande un nombre x à l'utilisateur et qui retourne la valeur absolue de ce nombre :
        
        * la valeur absolue de 5 est 5 
        * la valeur absolue de -3 est 3
        * la valeur absolue de 20 est 20
        * la valeur absolue de -4 est 4
        * la valeur absolue de 0  est 0

        Compléter le programme suivant pour qu'il affiche la valeur absolue du nombre saisi par l'utilisateur. 
        
        {{IDE('script3.6')}}

    === "Solution"

        Correction de l'exercice : 
        
        ```python 
        def valeur_absolue(x):
            if x > 0 :
                va = x
            elif x == 0 :
                va = 0
            else :
                va = -x
            return va
        ```

???+ question "Exercice 7"

	=== "Énoncé"

        Compléter la fonction `donner_categorie`  d'argument  l’âge d’un enfant et qui retourne sa catégorie :        
        * Poussin » de 6 à 7 ans
        * Pupille » de 8 à 9 ans
        * Minime » de 10 à 11 ans
        * Cadet » après 12 ans
        
        
        {{IDE('script3.7')}}

    === "Solution"

        Correction de l'exercice : 

        ```python 
        def donner_categorie(age):
            if age >= 6 and age <= 7 :
                return "Poussin"
            elif age >= 8 and age <= 9 :
                return "Pupille"
            elif age >= 10 and age <= 11 :
                return "Minime"
            elif age >= 12 :
                return "Cadet"
        ```
        

???+ question "Exercice 8"

	=== "Énoncé"

        Pour simplifier, on suppose  qu'une année est bissextile si elle est un multiple de 4 
        
        Écrire une fonction qui prend en argument une année et qui affiche " bissextile " ou "non bissextile"  en fonction de l'année saisie.

        **Remarque**  : Il est aussi possible que la fonction `est_bissextile` retourne True ou False .
        {{IDE('script3.8')}}

    === "Solution"
    
        Correction de l'exercice : 
        
        ```python
        def est_bissextile(x):
            if x %4 == 0 :
                return "C'est une année bissextile"
            else :
                return " Ce n'est pas une année bissextile"

        ```

???+ question "Exercice 9"

	=== "Énoncé"

        Compléter la fonction `reciproque_pythagore` qui prend en argument les 3 longueurs d'un triangle ( Le premier nombre étant la longueur du plus grand côté) et qui retourne True si le triangle est rectangle et False sinon. 

        **Remarque** : Les informations au début de la fonction sont des  commentaires destinés à la compréhension de son fonctionnement.

        {{IDE('script3.9')}}

    === "Solution"
    
        Correction de l'exercice : 
        
        ```python
        def reciproque_pythagore(cote1, cote2, cote3):
            '''
            cote1 est la longueur du plus grand coté
            cote2 est la longueur du deuxième coté
            cote3 est la longueur du troisième coté

            la fonction retourne True si le triangle est rectangle et False sinon.
            '''
            if cote1 **2 == cote2 **2 + cote3**2 :

                return True
            else : 
                
                return False

        print(reciproque_pythagore(5,4,3))  
        print(reciproque_pythagore(10,7,7))      
        ```

???+ question "Exercice 10"

	=== "Énoncé"

        Fonction définie par morceaux

        On peut définir un fonction  f par morceaux. Pour calculer l'image d'un nombre par f on regarde à quel intervalle appartient ce nombre et on calcule l'image de ce nombre à l'aide de la bonne expression . 

        On donne  : 

        ![](./img/exo3.10.png)

        * calculer les images de -2, 1.5 et 3 par f

        * écrire une fonction python f ci-dessous et vérifier vos résultats

        

        {{IDE()}}

    === "Solution"
    
        Correction de l'exercice : 
        
        ```python

        def f(x) :
            if x < = 1 : 
                return 2*x+3 
            elif x > 1 and x <2  :
                return 5*x
            else :
                return 14 - 2*x

        print(f(-2), f(1.5),f(3))
        ```
???+ question "Exercice 11"

	=== "Énoncé"

        On considère la fonction f définie par $f(x) = x^2 +1$

        * Écrire la fonction f en python . 
        * Exécuter cette fonction pour plusieurs valeurs afin de trouver deux antécédents de 26 par f . 


        {{IDE('script3.8')}}

    === "Solution"
    
        Correction de l'exercice : 

        Les antécédents de 26 par f sont -5 et 5.
        
        ```python
        def f(x) : 
            return x**2 + 1
        
        print(f(-5) , f(5))
        ```