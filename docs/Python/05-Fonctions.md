---
author:  Equipe Mathématique Lycée Les Rimains
title: Fonctions
---
# Python : Les Fonctions



## I. Introduction

Jusqu'à présent, l'interaction avec l'utilisateur se fait à l'aide des fonctions ` input()` et ` print()`.

L'inconvénient de cette pratique est que le résultat affiché dans la console par la fonction   `print()` ne pourra pas être réutilisé. 

En Python, on peut définir des fonctions tout comme en mathématiques. 

Celles-ci nous permettront de reproduire des actions à plusieurs reprises en changeant les valeurs de départ. 

Voici la structure d'une fonction en Python : 

```python
def ma_fonction(argument1, argument2,..) :
    instructions 
    return resultat
```

!!! info "Remarques "
    * Une fonction peut n'avoir aucun argument 
    * Une fonction peut ne rien retourner 


## II.Exercices

???+ question "Exercice1"

    === "Énoncé"

        L'indice de masse corporelle (IMC) d'une personne est donnée par la formule $IMC = \frac{masse}{taille^2}$ où la masse est en kilogrammes et la taille en mètres.

        * Créer une fonction `calculer_imc` qui prend en arguments les variables `masse` et `taille` et qui retourne l'IMC.

        * Tester cette fonction pour connaître votre IMC.

        {{IDE('script2.1')}}
        
    === "Correction"

        Correction de l'exercice : 
        
        ```python

        def calculer_imc(masse, taille) :
            imc = masse/(taille)**2
            return imc
        
        print(calculer_imc(80 , 1.80 ))
        ```


???+ question "Exercice2"

    === "Énoncé"

        Créer une fonction `lancer_un_dé` qui ne prend pas d'arguments en entrée et retourne un nombre aléatoire entre 1 et 6 .

        Afin de programmer cette fonction, nous utiliserons la fonction `randint()` de la librairie random ( même principe que pour l'utilisation de la fonction `sqrt`)

        Exemple : `randint(20, 30)` retournera un entier aléatoire entre 20 et 30.


        {{IDE('script2.2')}}

    === "Correction"

        Correction de l'exercice : 

        ```python 

        from random import randint

        def lancer_un_dé ():
            return randint(1,6)

        print(lancer_un_dé())
        ```

???+ question "Exercice3"

    === "Énoncé"

        la fonction `bizarre` est donnée ci-dessous. 
        
        * Deviner le résultat retourné par l'instruction  : `bizarre(x, 2, y)`
    
        * Vérifier votre prédiction 

        {{IDE('script2.3')}}

    === "Correction"

        ```python
        >>> 11
        ```


    
???+ question "Exercice4"

    === "Énoncé"

        Les fonctions `f`  `g` sont  données ci-dessous. 
        
        * Deviner le résultat retourné par l'instruction  : `g(2)`
    
        * Vérifier votre prédiction 

        {{IDE('script2.4')}}
    
    === "Correction"

        ```python
        >>> 23
        ``` 

???+ question "Exercice5"

    === "Énoncé"

        * Calculer l'aire d'un triangle de base 7 cm et de hauteur 3 cm
        
        * Calculer l'aire d'un disque de rayon 3 cm
        
        * Calculer le volume d'un cylindre de rayon 3cm et de hauteur 10 cm
        
        * Compléter les fonctions ci-dessous ( aire_triangle, aire_disque et volume_cylindre)
    
        * Vérifier vos calculs précédents 

        {{IDE('script2.5')}}

    === "Correction"

        ```python
        from math import pi


        ####################################
        def aire_triangle ( base, hauteur) :
            aire = (base*hauteur)/2
            return aire

        print ("aire du triangle de base 7 et de hauteur 3 : " , aire_triangle(7,3))

        ####################################
        def aire_disque ( rayon) :
            aire = pi*rayon**2
            return aire

        print("aire du disque de rayon 3 : " , aire_disque(3))

        ####################################
        def volume_cylindre ( rayon , hauteur) :
            volume = hauteur * aire_disque(rayon)
            return volume
            
        print("volume du cylindre de rayon 3 et de hauteur 10  : " , volume_cylindre( 3, 10 ))


        ``` 

???+ question "Exercice6"

    === "Énoncé"

        On considère les points M( -2,4) et N ( 3, -1)

        * Calculer la distance MN

        * Calculer les coordonnées du milieu de [MN]

        * Compléter les fonctions `distance` et `milieu` pour qu'elles permettent de calculer la distance entre deux points et les coordonnées du milieu du segment.  
        

        {{IDE('script2.6')}}

    === "Correction"

        ```python
        from math import sqrt

        ########################################
        def distance(xA , yA, xB, yB) :
            d = sqrt((xB-xA)**2 + (yB-yA)**2)
            return d

        print(distance (-2 , 4, 3 ,-1))

        #######################################
        def milieu(xA , yA, xB, yB) :
            xI = (xA + xB) / 2
            yI = (yA + yB) /2
            return xI,yI

        print(milieu(-2 , 4, 3 ,-1))


        ``` 

???+ question "Exercice7"

    === "Énoncé"

        On souhaite créer un programme qui permet de déterminer la quantité et le prix de peintures nécessaires pour peindre en 2 couches les murs d’une pièce.

        On va utiliser :

        • une fonction AireRectangle qui dépend de deux paramètres a et b, les longueurs et les largeurs des rectangles et renvoie la surface d’un rectangle.

        • une fonction client qui dépend de 6 paramètres :
        
            - L la longueur de la pièce en mètre,

            - l la largeur de la pièce en mètre,
            
            - h la hauteur de la pièce en mètre,

            - F le nombre de fenêtres dans la pièce (une fenêtre rectangulaire standard a pour dimension 0,9 par 1,1
            mètres),
            
            - P le nombre de porte dans la pièce (une porte rectangulaire standard à pour dimension 0,9 par 2 mètres,

            - N le nombre de couches souhaité,
        
        Dans cette fonction,
        
            - une variable C reçoit 5 (la capacité en litre d’un pot de peinture),
            - une variable p reçoit 52.5 (le prix en euros d’un pot de peinture),
            - une variable S reçoit la surface à peindre, en m2 (on appellera la fonction AireRectangle),
            - une variable Peinture reçoit le nombre de pots nécessaires pour peindre la surface de la pièce, arrondie à l’unité ajouté de 1, le code à compléter :
            
            Peinture=round(...,0)+1
            
            
            - PrixPeinture reçoit le prix à payer par le client.
            
            - la fonction retourne Peinture,PrixPeinture, le code à saisir : return Peinture,PrixPeinture
            
            - Respecter l’indentation pour exécuter toutes les lignes du code précédent dans la fonction Client.
        
        
        La fiche technique d’un pot de peinture, précise qu’avec 1 Litre de peinture on peint 10 m2.
        
        1. Créer les deux fonctions.
        
        2. Enregistrer puis exécuter le programme.
        
        
        La pièce à peindre est rectangulaire, de côtés 6.3 mètres et 4.7 mètres, de hauteur 2.6 mètres, elle possède 4 murs, on compte deux fenêtres et une porte. Le client désire 2 couches.
        
        Appeler la fonction client avec les paramètres qui conviennent.
        
        4. Quelle quantité de peinture est nécessaire (en pot) ? Quel est le prix du client.
        
        5. Pour aller plus loin, demander à l’utilisateur (le client) de saisir lui même les paramètres, puis afficher le résultat
        de la fonction.
        

        {{IDE('')}}

    === "Correction"

        ```python
        
        
        ```
        


    
        