---
author:  Equipe Mathématique Lycée Les Rimains
title: Boucles
---

## Boucles


![](../images/ch6.png)

## I. Introduction 

L’une des principales utilités d’un programme informatique est de répéter plusieurs fois des opérations similaires. Ainsi, ce qui prendrait du temps, même avec une calculatrice, pourra s’effectuer très rapidement à l’aide d’un programme. Dans un algorithme, le fait de répéter plusieurs fois une même opération s’appelle une boucle. Il existe des boucles bornées et des boucles non bornées. Vous les avez déjà rencontrées au collège sous scratch !

![](./img/cours0.png)

## I. Boucles bornées

![](./img/cours1.png)

???+ question "Exemple 1"

	=== "Énoncé"

        Recopier le programmes suivant puis le tester :
        
        ![](./img/exemple1.png)

        Notez et commentez le résultat.
        
        
        {{IDE()}}

    === "Solution"

        Le programme afficher tous les entiers de 3 à 6
        

???+ question "Exemple 2"

	=== "Énoncé"

        Recopier le programmes suivant puis le tester :
        
        ![](./img/exemple2.png)

        Notez et commentez le résultat.
        
        
        {{IDE()}}

    === "Solution"

        Le programme afficher tous les entiers de 100 à 103
        
???+ question "Exemple 3"

	=== "Énoncé"

        Recopier le programmes suivant puis le tester :
        
        ![](./img/exemple3.png)

        Notez et commentez le résultat.
        
        
        {{IDE()}}

    === "Solution"

        Le programme afficher tous les entiers de 0 à 4
        
???+ question "Exemple 4"

	=== "Énoncé"

        Recopier le programmes suivant puis le tester :
        
        ![](./img/exemple4.png)

        Notez et commentez le résultat.
        
        
        {{IDE()}}

    === "Solution"

        Le programme afficher tous les entiers de 3 à 6

        |abs|ord|affichage|
        |---|---|---|
        |-2|-1|(-2;-1)|
        ||0|(-2;0)|
        ||1|(-2;1)|
        |-1|-1|(-1;-1)|
        ||0|(-1;0)|
        ||1|(-1;1)|
        |0|-1|(0;-1)|
        ||0|(0;0)|
        ||1|(0;1)|
        |1|-1|(1;-1)|
        ||0|(1;0)|
        ||1|(1;1)|
        |2|-1|(2;-1)|
        ||0|(2;0)|
        ||1|(2;1)|
        
      
        
!!! info "Bilan"

    - On utilise une boucle for lorsqu’on sait combien de fois on doit répéter l’opération.
    - L’incrémentation est automatique
    - Dans for i in range(a,b), i varie de a jusqu’à b-1
    - Dans for i in range(b), i varie de 0 à b-1

!!! info " A faire"
    Exercices : 9 à 13 et 52 à 56

## II. Boucles conditionnelles

![](./img/cours1.png)

???+ question "Exemple 1"

	=== "Énoncé"

        Recopier le programmes suivant puis le tester :
        
        ![](./img/exemple5.png)

        Notez et commentez le résultat.
        
        
        {{IDE()}}

    === "Solution"

        Résultat :
            pour  n = 32
            n**2 = 1024
        
        Explication : 
            Ce programme détermine la plus petite valeur  de n pour laquelle n**2 > 1000

        
???+ question "Exemple 2"

	=== "Énoncé"

        Recopier le programmes suivant puis le tester :
        
        ![](./img/exemple6.png)

        Notez et commentez le résultat.
        
        
        {{IDE()}}

    === "Solution"

        Résultat : 
            20

            1048576
        Explication : 
            
            Ce programme détermine la première valeur de n pour laquelle 2**n > 1000000

???+ question "Exemple 3"

	=== "Énoncé"

        Recopier le programmes suivant puis le tester :
        
        ![](./img/exemple7.png)

        Notez et commentez le résultat.
        
        
        {{IDE()}}

    === "Solution"

        Tableau de déroulement du programme: 

        |n|somme|
        |---|---|
        ||0|
        |0||
        ||0|
        |1||
        ||1|
        |2||
        ||3|
        |3||
        ||6|
        |4||
        ||10|
        |5||
        ||15|
        |6||
        ||21|
        |7||
        ||28|
        |8||
        ||36|
        |9||
        ||45|
        ||55|
        |11||
        

!!! info "Bilan"

    - Les boucle conditionnelle permette de répéter un action tant que la condition est réalisée .  
    - En pratique, on place dans la condition de la boucle la négation de l'objectif recherché 
    - si l'on souhaite à déterminer n pour que la somme soir supérieure à 50, réalisera une boucle  ' tant que sommme < 50
    - les conditions à retenir : 
        - strictement inférieur ou supérieur : > ; <
        - supérieur ou inférieur  ou égal  : >= ; <=
        - test d'égalité : ==
        - test d'inégalité : !=

!!! info " A faire"
    Exercices : 14 à 21 et 58 à 62

## Travaux pratiques par affinités 

???+ question "TP1"

	=== "Énoncé"

        Paul place 1500 euros à un taux d’intérêt mensuel de 1.45 % . 

        De son côté, Antoine place 1800 euros à un taux de 0.85 % .

        1- Déterminer à l’aide d’un programme python l’épargne de Paul et d’Antoine au bout de 120 mois. 
        2- Au bout de combien de mois l’épargne de Paul sera-t-elle supérieur à celle d’Antoine ?

        
        
        {{IDE()}}

    === "Solution"
        h[https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp1](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp1)


???+ question "TP2"

	=== "Énoncé"

        ![](./img/tp2.png)

        1-  Utiliser un programme python pour déterminer la somme des 100 premier nombres entiers au carré. Puis vérifier la formule de l’image ci-contre.

        2- Combien de carré successifs doit-on additionner au minimum pour  que leur somme soit supérieur à  ? 

        
        {{IDE()}}

    === "Solution"
        [https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp2](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp2)


???+ question "TP3"

	=== "Énoncé"

        ![](./img/tp3.png)

        L’objectif de cette activité est d’utiliser le module Turtle pour créer la spirale carré suivante sur votre calculatrice Numworks .

        La librairie Turtle permet de représenter des des figure géométriques sur l’écran de la calculatrice . 

        Pour utiliser la librairie Turtle, il est conseillé de consulter cette documentation :   https://www.numworks.com/fr/ressources/manuel/python/#le-module-turtle

    === "Solution"
        [https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp3](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp3)


        
???+ question "TP4"

	=== "Énoncé"

        ![](./img/tp4.2.jpg)

        Python ne peut pas générer nativement un nombre aléatoire. Comme pour l'utilisation des fonctions mathématiques, il faut utiliser une librairie pour générer des nombres aléatoires.

	    1a- lancer le programme suivant : 
        
        ![](./img/tp4.1.png)

        {{IDE()}}

	    - Expliquer le résultat de la fonction random() ?

        - Déplacement d'un kangourou !
        
        Un kangourou se déplace sur un axe gradué illimité en partant de 0. 

        Son déplacement consiste à faire une succession de sauts d'une unité à chaque fois vers la gauche ou vers la droite. 
        
        Cependant, il a une légère préférence pour le saut « à droite » qui fait que la probabilité qu'il saute « à droite » est de 0,8 alors que celle du saut « à gauche » est de 0,2 . 


        - La kangourou effectue 100 sauts .
        On cherche à estimer sa position moyenne au bout des 100 sauts .


        En utilisant les éléments de la partie A, créez un programme simule les 100 sauts de la puce et qui affiche la position du kangourou. 

        - Réaliser un programme qui effectue 1000 fois l’expérience et qui mesure la position moyenne du kangourou à la fin de chaque expérience . 

    === "Solution"
        [https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp4](https://my.numworks.com/python/morelo-lolo/sec_algo_boucles_tp4)




        