def f(x) :
    return 4*x + 3


def g(x) :
    y = 1 + x**2
    return f(y)


print(g(2))

