# TP Zéro

!!! abstract "Objectifs"
    L'objectif de ce TP est de se familiariser avec les différents outils numériques du lycée que vous utiliserez en SNT cette année, mais également dans beaucoup d'autres disciplines:

    - l'ordinateur
    - le réseau interne
    - l'ENT et ses services
    - ce site
    - l'application mobile 

!!! example "Consignes"

    1. Ouvrir le gestionnaire de fichiers, puis aller dans le lecteur `\perso`, puis le dossier `Mes Documents`. C'est le dossier dans lequel seul vous avez accès, et dans lequel nous vous conseillons d'enregistrer vos documents numériques. Ailleurs que dans `\perso`, ils risquent d'être perdus.

    2. Créer un nouveau dossier nommé `SNT`.

    3. Ouvrir le logiciel LibreOffice Writer, et notez vos nom et prénoms dans le document vierge. Puis sauvegardez-le sous la forme `SNT_TP0_nom_prenom.odt` dans votre dossier `\perso\Mes Documents\SNT`. **Ne pas fermer le logiciel**.

        ![](../../images/meme_tp0.jpg){: .center width=480} 

    4. Dans les applications de l'ordinateur, chercher et lancer l'outil de capture d'écran. Faire une capture de votre dossier contenant le fichier créé (pas tout l'écran, sélectionnez une zone adéquate) et l'enregistrer dans votre `\perso\Mes Documents`.

    5. Insérer cette capture d'écran dans votre document `SNT_TP0_nom_prenom.odt` puis sauvegarder.

    6. Lancez Firefox et rendez-vous sur le cloud des Rimains  [https://cloud.lesrimains.org](https://cloud.lesrimains.org). Déplacez-vous dans les fichiers et retrouvez votre fichier `SNT_TP0_nom_prenom.odt` . Téléchargez-le et ouvrez-le pour le modifier en ajouter la ligne 'modification depuis le cloud'. 

        ![](../../images/cloud_lesrimains.png){: .center}  


    7. Convertir le document en PDF sous le même nom (avec l'extension `.pdf` plutôt que `.odt` bien entendu) en passant pour cela soit par le menu **Fichier** puis **Exporter vers** et enfin **Exporter directement en PDF**, soit en cliquant sur l'icone correspondante:

        ![](../../images/icone_PDF.png){: .center } 

    8. Déposez le document PDF dans le dossier «Seconde\Restitution de Devoirs\SNT\TP zéro» .

    9. Connectez-vous à Ecole Directe et envoyez-moi un message pour m'indiquer que vous avez-bien déposé le document.

         ![](../../images/application nextcloud telephone.png){: .center } 

    10. **Installation de l'application Nextcloud** sur le téléphone. Télécharger l'application depuis votre plateforme habituelle de téléchargement.
    
    11. Lancer l'application. 
    
    12. Voici le lien https à renseigner : [https://cloud.lesrimains.org](https://cloud.lesrimains.org).
    
    13. Vous pouvez maintenant vous connecter avec votre identifiant et votre mot de passe. 
    
